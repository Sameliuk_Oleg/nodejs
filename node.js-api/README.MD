# Node.js-api
## Instructions for using the application
1.1 Run the application in the CLI using **_`node task.js`_**

1.2 After launching the program you will have 3 options.

    1.2.1   If you want to know about your OS, press 1
            After that, you will receive information about the OS,
          and you will be prompted to exit the program on button 4.
    1.2.2   If you want to get information about the URL that interests
          you, click 2
            You will then be prompted to enter the URL you would 
          like to check.
            After completing the above, you will receive the 
          result of the request, and you will be prompted to 
          exit the program on button 4.
    1.2.3   To create an additional process that will start 
          another JS file, press 3
            After that the result of additional process script.js
          will be deduced.
            You will also be prompted to exit the program on button 4