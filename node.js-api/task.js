import urlUtils from 'url';
import queryStrings from 'querystring';
import readline from "readline";
import os from 'os';
import {spawn} from "child_process"

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("\n" + "If you want to know about your OS, press 1 " +
    "\n" + "If you want to get information about the URL that interests you, click 2 " +
    "\n" + "To spawn an additional process that will start the execution of another JS file, press 3 ",
    function (name) {

        if (name === '1') {
            console.log(`${name}, your OS`, os.platform(), os.cpus());
            rl.question("\n" + "If you want to exit the program, press 4  ", function (name) {
                if (name === "4") {
                    process.exit(0);
                }
            })

        } else if (name === "2") {
            rl.question("\n" + "Enter the URL that interests you ", function (name) {
                const url = `${name}`;
                const parsedUrl = urlUtils.parse(url);
                const parsedQueryString = queryStrings.parse(parsedUrl.query || '');
                console.log(parsedUrl);
                console.log(parsedQueryString);
                rl.question("\n" + "If you want to exit the program, press 4  ", function (name) {
                    if (name === "4") {
                        process.exit(0);
                    }
                })
            })
        } else if (name === "3") {
            const child = spawn('node', ['script.js']);
            child.stdout.on('data', data => {
                console.log("\n" + `${data}`)
            });
            rl.question("\n" + "If you want to exit the program, press 4  ", function (name) {
                if (name === "4") {
                    process.exit(0);
                }
            })

        } else if (name === "4") {
            process.exit(0);
        }


    });