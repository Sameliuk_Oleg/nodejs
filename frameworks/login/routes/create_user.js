let express = require('express');
let router = express.Router();

global.checkAdministrator = (request, response, next) => {
    const token = request.get('Authentication');
    if (token === global.token) {
        next();
    }
    response.status(401).end();
};

router.get('/', global.checkAdministrator, (request, response) => {
    UsersTable.build({
        user: request.body.login,
        password: request.body.password,
        role: request.body.role
    }).save().then(() => {
        console.log('New profile saved')
    });

    response.send({
            "new user": request.body.login,
            "new user password": request.body.password,
            "new user role": request.body.role
        }
    )
});

module.exports = router;

