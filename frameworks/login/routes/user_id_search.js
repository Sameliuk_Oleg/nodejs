let express = require('express');
const app = require("../app");
let router = express.Router();


let noteID;
let noteUser;
let notePassword;
let noteRole;

router.get('/', global.checkAdministrator, (request, response) => {
    UsersTable.findOne({where: {id: request.body.id}}).then(users => {
            let note = users.get({plain: true})
            noteID = JSON.stringify(note.id);
            noteUser = JSON.stringify(note.user);
            notePassword = JSON.stringify(note.password);
            noteRole = JSON.stringify(note.role)
        }
    )

    response.send({
            "id": noteID,
            "user": noteUser,
            "password": notePassword,
            "role": noteRole
        }
    );

})
module.exports = router


