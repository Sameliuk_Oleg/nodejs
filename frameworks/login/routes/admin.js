let express = require('express');
let router = express.Router();
let Sequelize = require('sequelize');
let fs = require('fs');
let json = JSON.parse(fs.readFileSync('package.json', 'utf8'))

global.sequelize = new Sequelize(`${json.database}`, `${json.user}`, `${json.password}`, {
    host: 'localhost',
    logging: false,
    dialect: 'mysql'
});

global.UsersTable = sequelize.define('usersTable', {
    user: Sequelize.STRING,
    password: Sequelize.STRING,
    role: Sequelize.STRING
})


const users = [
    {
        user: 'admin',
        password: 'pass',
        role:'Administrator'
    }
];

sequelize.sync({force: true}).then(() => {
    UsersTable.bulkCreate(users, {validate: true}).then()
});

let admin = users[0].user;
let password = users[0].password;
global.token = 'admin_token';

router.post('/', ((request, response) => {
    if (request.body.login === admin && request.body.password === password) {
        response.setHeader('Authentication', global.token)
        response.send({
            token: global.token
        })
    } else {
        response.status(401).end();
    }
}))

module.exports = router;