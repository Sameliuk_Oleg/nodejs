let express = require('express');
let router = express.Router();

router.get('/', checkAdministrator, (request, response) => {
    const allRow = async (UsersTable) => {
        let data = await UsersTable.findAll({raw: true});
        console.log(data);
    };

    response.send(
        {
            "allUsers": allRow(UsersTable)
        }
    )
});

module.exports = router;



