let express = require('express');
let router = express.Router();

router.get('/', global.checkAdministrator, (request, response) => {
    UsersTable.destroy({where: {id: request.body.id}});

    response.send({
            "delete user": request.body.id,
        }
    )
});

module.exports = router;

