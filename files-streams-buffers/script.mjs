import fs from 'fs'
import readline from 'readline';
import http from 'http';

import('log-timestamp');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const hostname = '127.0.0.1';
const port = 3000;
const server = http.createServer((request, response) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');
    fs.readFile('changes.json', 'utf8', (error, data) => {
        if (error) {
            throw error
        }
        response.end('Read file changes \n' + data);
    })
});
let prevDataFile, prevNameFile, fileName, data, dataInformation;
let checkFile = false;

const readFileStream = () => {
    const reader = fs.createReadStream(`data/${fileName}`, {
        flag: 'a+',
        encoding: 'UTF-8',
        start: 1,
        end: 100000,
        highWaterMark: 100000
    });
    reader.setMaxListeners(1000);

    reader.on('data', function (chunk) {
        if (prevDataFile.includes(chunk)) {
            return false;
        } else {
            fs.appendFile('changes.json', JSON.stringify(dataInformation),
                (err) => {
                    data = fs.statSync(`data/${fileName}`);
                    dataInformation = {
                        FileSize: data.size,
                        FileBirth: data.birthtime,
                        TheCurrentTimeIs: data.mtime
                    }
                    console.log('File was changed and info about changes add');
                });
        }
    });
    reader.on('end', () => {
    });
}

server.listen(port, hostname, () => {
    console.log(`http://${hostname}:${port}/`);
    fs.truncate('changes.json', 0, () => console.log('File with changes info was clear.'))
    console.log('Please, enter path to file.');

    rl.on('line', (name) => {
        fileName = name;
        console.log(`Start check file changes on ${fileName}`);
        data = fs.statSync(`data/${fileName}`);
        dataInformation = {
            FileSize: data.size,
            FileBirth: data.birthtime,
            TheCurrentTimeIs: data.mtime,
            Event: 'changed file'
        };

        fs.readFile(`data/${fileName}`, 'utf8',
            (error, data) => {
                if (error) {
                    throw error
                }
                prevDataFile = data;
            });

        fs.readdir('data/', (error, files) => {
            if (error) {
                throw error
            }
            files.forEach(file => {
                prevNameFile = file;
            });
        });

        checkFile = setInterval(() => {
            testAddChanges(fileName);
            testRenameFile('news.txt', 'rename.txt');
            readFileStream();
            testRenameFile('rename.txt', 'news.txt');
            checkFile = false;
        }, 6000);
    });
});

const testAddChanges = (name) => {
    fs.appendFile(`data/${name}`, 'test test test',
        (err) => {
            console.log('File was changed.');
        });
}

const testRenameFile = (name, rename) => {
    fs.rename(`data/${name}`, `data/${rename}`, () => {
        console.log(`File was renamed. `);
        fileName = rename;
    });
    fs.readdir('data/', (error, allFiles) => {
        if (error) {
            throw error
        }
        if (allFiles.includes(prevNameFile)) {
            return false;
        } else {
            fs.appendFile('changes.json', JSON.stringify(dataInformation),
                (err) => {
                    data = fs.statSync(`data/${fileName}`);
                    dataInformation = {
                        FileSize: data.size,
                        FileBirth: data.birthtime,
                        TheCurrentTimeIs: data.mtime,
                        Event: 'rename file'
                    }
                    console.log('File was renamed and info about rename add.');
                });
        }
    });
}

